# based on
# https://pastebin.com/xJBLcANY
import os

from Crypto.Cipher import AES, PKCS1_OAEP
from Crypto.PublicKey import RSA

from settings import PRIVATE_KEY_FILE, CRYPT_DIRECTORY


def decrypt(file, private_key):
    with open(file, "rb") as file_in:
        enc_session_key, nonce, tag, ciphertext = [file_in.read(x) for x in (private_key.size_in_bytes(), 16, 16, -1)]

    cipher_rsa = PKCS1_OAEP.new(private_key)
    session_key = cipher_rsa.decrypt(enc_session_key)
    cipher_aes = AES.new(session_key, AES.MODE_EAX, nonce)
    data = cipher_aes.decrypt_and_verify(ciphertext, tag)

    with open(str(file[:-4]), "wb") as file_out:
        file_out.write(data)
        print(file, "decrypted!")

    os.remove(file)


def walk(dir, private_key=None):
    if not private_key:
        with open(PRIVATE_KEY_FILE) as f:
            private_key = RSA.import_key(f.read())

    for name in os.listdir(dir):
        path = os.path.join(dir, name)
        if os.path.isfile(path):
            decrypt(path, private_key)
        else:
            walk(path, private_key)


walk(CRYPT_DIRECTORY)
print("-" * 60)
