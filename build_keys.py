# based on
# https://pastebin.com/uSBL0f49
from Crypto.PublicKey import RSA

from settings import PRIVATE_KEY_FILE, PUBLIC_KEY_FILE, KEY_BITS

private_key = RSA.generate(KEY_BITS)
with open(PRIVATE_KEY_FILE, 'wb') as private:
    private.write(private_key.export_key('PEM'))
    print('private key generated')

public_key = private_key.publickey().export_key('PEM')
with open(PUBLIC_KEY_FILE, "wb") as public:
    public.write(public_key)
    print('public key generated')
